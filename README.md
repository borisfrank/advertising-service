[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Java17](https://img.shields.io/badge/java-17-blue)](https://img.shields.io/badge/java-17-blue)
[![Maven](https://img.shields.io/badge/maven-v3.8.6-blue)](https://img.shields.io/badge/maven-v3.8.6-blue)

# Advertising Service
A simple online advertising service

## Environment requirements

For building and running the application you need:

- [git-smc](https://git-scm.com/)  2.x or later.
- [OpenJDK 17](https://adoptium.net/releases.html?variant=openjdk17&jvmVariant=hotspot)
- [Maven 3](https://maven.apache.org)
- [docker.io](https://linuxconfig.org/how-to-install-docker-on-ubuntu-20-04-lts-focal-fossa) 20.10.7  or later. (Optional)
- [docker-compose](https://linuxconfig.org/how-to-install-docker-compose-on-ubuntu-20-04-focal-fossa-linux) 1.25.0 or later. (Optional)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.borisfrank.advertisingservice.AdvertisingServiceApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

This will run the application using the `application-dev.properties` which is already configured to use [H2 Database](https://www.h2database.com/html/main.html).

### Run via docker
First, you need to create `.env` file in the root project folder and populate it with the content of `.env.example`.
After that, you need to provide value for empty variables.
Now you can run this command to build the project

```shell
mvn clean install
```

And then

```shell
docker-compose up -d
```

to deploy the application.

### Test the application
Open your fivorite browser and navigate to [http://localhost:8080/swagger-ui/index.html#/](http://localhost:8080/swagger-ui/index.html#/) to view documentation of all application api endpoints.


