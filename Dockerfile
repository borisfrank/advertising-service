FROM openjdk:17-alpine
VOLUME /temp
ADD /target/*.jar /app.jar
WORKDIR /
ENTRYPOINT ["java", "-jar", "/app.jar"]