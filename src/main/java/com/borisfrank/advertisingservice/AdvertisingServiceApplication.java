package com.borisfrank.advertisingservice;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(
		info = @Info(
				title = "Advertising service",
				version = "1.0.0",
				description = "A simple online advertising service",
				termsOfService = "borisfrank",
				contact = @Contact(
						name = "Boris Frank DAKLEU YAPPI",
						email = "frankborisdakleu@gmail.com"
				)
		)
)
@SpringBootApplication
public class AdvertisingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdvertisingServiceApplication.class, args);
	}

}
