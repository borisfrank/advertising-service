package com.borisfrank.advertisingservice.converter;

import com.borisfrank.advertisingservice.model.ListingState;
import org.springframework.core.convert.converter.Converter;

public class StringToListingStateConverter implements Converter<String, ListingState> {
    @Override
    public ListingState convert(String source) {
        try {
            return ListingState.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException ex) {
            return null;
        }
    }
}
