package com.borisfrank.advertisingservice.security.filter;

import java.io.IOException;

import com.borisfrank.advertisingservice.security.jwt.JwtUtils;
import com.borisfrank.advertisingservice.security.service.UserDetailServiceImpl;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
public class JwtAuthorizationFilter extends OncePerRequestFilter {

    private final UserDetailServiceImpl userDetailService;
    private final JwtUtils jwtUtils;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final String authToken = jwtUtils.getJwtFromRequest(request);
        // On verifie que le token contient le token et que celui-ci est valide
        if (StringUtils.hasText(authToken) && jwtUtils.isValidToken(authToken)) {
            log.info("Authorization trouvé!");
            try {
                // On recupère le username contenu dans le token
                String username = jwtUtils.getSubject(authToken);
                // On verifie qu le compte existe à partir du username
                UserDetails userDetails = this.userDetailService.loadUserByUsername(username);
                // On crée une authentification pour ce username
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, null, userDetails.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                // On specifie au contexte de Spring Security que l'utilisateur est authentifié
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            } catch (Exception ex) {
                log.error("Impossible d'ajouter l'authentification au contexte de Spring Security", ex);
            }
        } else {
            log.info("Authorization non trouvé!");
        }
        filterChain.doFilter(request, response);
    }
}
