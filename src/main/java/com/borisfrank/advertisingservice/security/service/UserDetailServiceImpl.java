package com.borisfrank.advertisingservice.security.service;

import com.borisfrank.advertisingservice.model.Dealer;
import com.borisfrank.advertisingservice.repository.DealerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService {

    private final DealerRepository dealerRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Dealer dealer = dealerRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
        // On ne gere pas les roles pour le moment. Du coup on pass un tableau vide a la place des authorisations
        return new User(dealer.getUsername(), dealer.getPassword(), Collections.emptyList());
    }
}
