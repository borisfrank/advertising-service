package com.borisfrank.advertisingservice.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class JwtUtils {

    private final JwtConfig jwtConfig;

    public Map<String, String> generateAccessAndRefreshTokens(String username) {
        String jwtAccessToken = this.generateAccessToken(username);
        Map<String, String> idToken = new HashMap<>();
        idToken.put("access_token", jwtAccessToken);
        idToken.put("token_type", jwtConfig.getPrefix().trim());
        return idToken;
    }

    public String generateAccessToken(String username) {
        return JWT.create()
                .withSubject(username)
                .withExpiresAt(new Date(System.currentTimeMillis() + jwtConfig.getAccessTokenExpirationInMs()))
                .sign(this.getAlgorithm());
    }

    public String getSubject(String authToken) {
        DecodedJWT decodedJWT = JWT.require(getAlgorithm()).build().verify(authToken);
        return decodedJWT.getSubject();
    }

    public boolean isValidToken(String authToken) {
        try {
            JWT.require(getAlgorithm()).build().verify(authToken);
            return true;
        } catch (JWTVerificationException ex) {
            log.error("Token JWT non valide", ex);
        } catch (IllegalArgumentException ex) {
            log.error("Les données du token JWT sont vides.", ex);
        }
        return false;
    }

    private Algorithm getAlgorithm() {
        return Algorithm.HMAC256(jwtConfig.getSecret());
    }

    public String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader(jwtConfig.getHeader());
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(jwtConfig.getPrefix())) {
            return bearerToken.substring(jwtConfig.getPrefix().length());
        }
        return null;
    }
}
