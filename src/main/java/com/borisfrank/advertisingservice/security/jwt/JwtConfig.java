package com.borisfrank.advertisingservice.security.jwt;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class JwtConfig {

    @Value("${app.jwt.header}")
    private String header;

    @Value("${app.jwt.prefix}")
    private String prefix;

    @Value("${app.jwt.secret}")
    private String secret;

    @Value("${app.jwt.access-token.expirationInMs}")
    private int accessTokenExpirationInMs;
}
