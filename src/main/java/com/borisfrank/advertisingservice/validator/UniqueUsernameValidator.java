package com.borisfrank.advertisingservice.validator;

import com.borisfrank.advertisingservice.service.DealerService;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@RequiredArgsConstructor
public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {
    private final DealerService dealerService;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        // La valeur n'est pas vide et il n'existe pas d'utilisateur en base avec le meme username.
        return StringUtils.hasText(value) && !dealerService.isUsernameUsed(value);
    }
}
