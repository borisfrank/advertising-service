package com.borisfrank.advertisingservice.mapper;

import com.borisfrank.advertisingservice.model.Listing;
import com.borisfrank.advertisingservice.payload.request.ListingRequest;
import com.borisfrank.advertisingservice.payload.response.ListingResponse;

public class ListingMapper {

    private ListingMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static ListingResponse mapToListingResponse(Listing listing){
        if (listing == null) {
            return null;
        }
        return ListingResponse.builder()
                .id(listing.getId())
                .vehicle(listing.getVehicle())
                .price(listing.getPrice())
                .createdAt(listing.getCreatedAt())
                .publishedAt(listing.getPublishedAt())
                .state(listing.getState())
                .build();
    }

    public static Listing mapToListing(ListingRequest request){
        if (request == null) {
            return null;
        }
        return Listing.builder()
                .vehicle(request.getVehicle())
                .price(request.getPrice())
                .build();
    }
}
