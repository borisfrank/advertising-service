package com.borisfrank.advertisingservice.mapper;

import com.borisfrank.advertisingservice.model.Dealer;
import com.borisfrank.advertisingservice.model.Listing;
import com.borisfrank.advertisingservice.payload.request.ListingRequest;
import com.borisfrank.advertisingservice.payload.request.RegisterRequest;
import com.borisfrank.advertisingservice.payload.response.ListingResponse;

public class AuthMapper {

    private AuthMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static Dealer mapToDealer(RegisterRequest request){
        if (request == null) {
            return null;
        }
        return Dealer.builder()
                .name(request.getName())
                .username(request.getUsername())
                .build();
    }
}
