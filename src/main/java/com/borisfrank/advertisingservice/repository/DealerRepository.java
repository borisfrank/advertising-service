package com.borisfrank.advertisingservice.repository;

import com.borisfrank.advertisingservice.model.Dealer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DealerRepository extends JpaRepository<Dealer, Long> {
    Optional<Dealer> findByUsername(String username);
    Boolean existsByUsername(String username);
}
