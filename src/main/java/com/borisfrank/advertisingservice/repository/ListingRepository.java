package com.borisfrank.advertisingservice.repository;

import com.borisfrank.advertisingservice.model.Dealer;
import com.borisfrank.advertisingservice.model.Listing;
import com.borisfrank.advertisingservice.model.ListingState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ListingRepository extends JpaRepository<Listing, Long> {
    Optional<Listing> findByIdAndDealer(Long id, Dealer dealer);
    @Query(
            value = "SELECT l.* FROM listings l WHERE l.dealer_id = :dealerId AND l.published_at IS NOT NULL ORDER BY l.published_at ASC LIMIT 1",
            nativeQuery = true
    )
    Optional<Listing> findOldPublishedAtByDealderId(Long dealerId);
    List<Listing> findByState(ListingState state);
    List<Listing> findByDealer(Dealer dealer);
    List<Listing> findByDealerAndState(Dealer dealer, ListingState state);
    int countByDealerAndState(Dealer dealer, ListingState state);
}
