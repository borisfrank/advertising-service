package com.borisfrank.advertisingservice.payload.request;

import com.borisfrank.advertisingservice.model.ListingState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetListingRequest {

    private ListingState state;
}
