package com.borisfrank.advertisingservice.payload.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ListingRequest {

    @NotBlank(message = "Le nom du vehicule ne doit pas etre vide")
    @NotNull(message = "Le nom du vehicule ne doit pas etre nul")
    @Size(max = 50, message = "Le nom du vehicule doit avoir une longeur maximale de 50 caracteres")
    private String vehicle;

    @NotBlank(message = "Le prix du vehicule ne doit pas etre vide")
    @NotNull(message = "Le prix du vehicule ne doit pas etre nul")
    @Size(message = "Le prix du vehicule doit supérieur ou egal à 0")
    private double price;
}
