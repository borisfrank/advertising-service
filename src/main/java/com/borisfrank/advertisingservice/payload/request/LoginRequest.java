package com.borisfrank.advertisingservice.payload.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginRequest {

    @NotBlank(message = "Le nom d'utilisateur ne doit pas etre vide")
    @NotNull(message = "Le nom d'utilisateur ne doit pas etre nul")
    @Size(max = 50, message = "Le nom d'utilisateur doit avoir une longeur maximale de 50 caracteres")
    private String username;

    @NotBlank(message = "Le mot de passe ne doit pas etre vide")
    @NotNull(message = "Le mot de passe ne doit pas etre nul")
    @Size(max = 50, message = "Le mot de passe doit avoir une longeur maximale de 50 caracteres")
    private String password;
}
