package com.borisfrank.advertisingservice.payload.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PublishListingRequest {

    private boolean unpiblishOldOnLimitReached;
}
