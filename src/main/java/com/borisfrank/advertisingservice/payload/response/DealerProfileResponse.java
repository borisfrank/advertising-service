package com.borisfrank.advertisingservice.payload.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.Date;


@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DealerProfileResponse {
    private Long id;

    private String name;

    private String username;

    private Date createdAt;

    private int publishedLimit;

    private boolean unpiblishOldOnLimitReached;
}
