package com.borisfrank.advertisingservice.payload.response;

import com.borisfrank.advertisingservice.model.ListingState;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListingResponse {

    private Long id;

    private double price;

    private String vehicle;

    private ListingState state;

    private Date createdAt;

    private Date publishedAt;
}
