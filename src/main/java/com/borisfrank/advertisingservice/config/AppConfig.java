package com.borisfrank.advertisingservice.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class AppConfig {

    @Value("${app.dealer.publishedLimit}")
    private int defaultPublishedLimit;
}
