package com.borisfrank.advertisingservice.controller;

import com.borisfrank.advertisingservice.exception.UserNotFoundException;
import com.borisfrank.advertisingservice.payload.request.LoginRequest;
import com.borisfrank.advertisingservice.payload.request.RegisterRequest;
import com.borisfrank.advertisingservice.payload.response.AuthResponse;
import com.borisfrank.advertisingservice.service.AuthService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/auth")
@Slf4j
public class AuthController {

    private final AuthService authService;

    @PostMapping("login")
    public ResponseEntity<AuthResponse> authentication(@RequestBody @Valid LoginRequest request) throws AuthenticationException {
        try {
            AuthResponse authResponse = authService.login(request);
            log.info("Utilisateur authentifié avec succès: {}", request.getUsername());
            return ResponseEntity.ok(authResponse);
        } catch (UsernameNotFoundException ex) {
            throw new BadCredentialsException("");
        }
    }

    @PostMapping("register")
    public ResponseEntity<AuthResponse> register(@RequestBody @Valid RegisterRequest request) throws Exception {
        try {
            AuthResponse authResponse = authService.register(request);
            log.info("Compte utilisateur créé avec succès: {}", request.getUsername());
            return ResponseEntity.status(HttpStatus.CREATED).body(authResponse);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
