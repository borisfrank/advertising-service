package com.borisfrank.advertisingservice.controller;

import com.borisfrank.advertisingservice.model.ListingState;
import com.borisfrank.advertisingservice.payload.request.ListingRequest;
import com.borisfrank.advertisingservice.payload.request.PublishListingRequest;
import com.borisfrank.advertisingservice.payload.response.ListingResponse;
import com.borisfrank.advertisingservice.service.ListingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/listing")
public class ListingController {

    private final ListingService listingService;

    @PostMapping
    public ResponseEntity<ListingResponse> saveListing(@RequestBody ListingRequest request) {
        ListingResponse response = listingService.saveListing(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @GetMapping
    public ResponseEntity<List<ListingResponse>> getAllListing(@RequestParam(required = false, name = "state") ListingState state) {
        List<ListingResponse> response = listingService.getAllListing(state);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ListingResponse> updateListing(@PathVariable Long id, @RequestBody ListingRequest request) {
        ListingResponse response = listingService.updateListing(id, request);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/{id}/publish")
    public ResponseEntity<String> publishListing(@PathVariable Long id, @RequestBody PublishListingRequest request) {
        String response = listingService.publishListing(id, request);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/{id}/unpublish")
    public ResponseEntity<String> unpublishListing(@PathVariable Long id) {
        String response = listingService.unpublishListing(id);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> destroyListing(@PathVariable Long id) {
        String response = listingService.destroyListing(id);
        return ResponseEntity.ok(response);
    }
}
