package com.borisfrank.advertisingservice.model;

public enum ListingState {
    DRAFT, PUBLISHED
}
