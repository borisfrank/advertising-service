package com.borisfrank.advertisingservice.service;

import com.borisfrank.advertisingservice.payload.request.LoginRequest;
import com.borisfrank.advertisingservice.payload.request.RegisterRequest;
import com.borisfrank.advertisingservice.payload.response.AuthResponse;

public interface AuthService {
    AuthResponse login(LoginRequest request);
    AuthResponse register(RegisterRequest request);
}
