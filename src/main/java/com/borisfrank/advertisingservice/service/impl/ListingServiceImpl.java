package com.borisfrank.advertisingservice.service.impl;

import com.borisfrank.advertisingservice.exception.ListingLimitReachedException;
import com.borisfrank.advertisingservice.exception.ListingNotFoundException;
import com.borisfrank.advertisingservice.mapper.ListingMapper;
import com.borisfrank.advertisingservice.model.Dealer;
import com.borisfrank.advertisingservice.model.Listing;
import com.borisfrank.advertisingservice.model.ListingState;
import com.borisfrank.advertisingservice.payload.request.ListingRequest;
import com.borisfrank.advertisingservice.payload.request.PublishListingRequest;
import com.borisfrank.advertisingservice.payload.response.ListingResponse;
import com.borisfrank.advertisingservice.repository.DealerRepository;
import com.borisfrank.advertisingservice.repository.ListingRepository;
import com.borisfrank.advertisingservice.service.ListingService;
import com.borisfrank.advertisingservice.util.WebSecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class ListingServiceImpl implements ListingService {
    private final ListingRepository listingRepository;
    private final DealerRepository dealerRepository;
    private final WebSecurityUtils securityUtils;

    @Override
    public ListingResponse saveListing(ListingRequest request) {
        // Recupération du dealer actif
        Dealer dealer = dealerRepository.findByUsername(securityUtils.getCurrentUsername()).orElseThrow(() -> new RuntimeException("Une erreur est survenue. Merci de reessayer"));
        log.info("Current Dealer ID: " + dealer.getId());
        Listing listing = ListingMapper.mapToListing(request);
        listing.setDealer(dealer);
        log.info("Listing ajouté avec succès: {}", listing.getId());
        return ListingMapper.mapToListingResponse(listingRepository.save(listing));
    }

    @Override
    public List<ListingResponse> getAllListing(ListingState state) {
        // Recupération du dealer actif
        Dealer dealer = dealerRepository.findByUsername(securityUtils.getCurrentUsername()).orElseThrow(() -> new RuntimeException("Une erreur est survenue. Merci de reessayer"));
        log.info("Current Dealer ID: " + dealer.getId());
        List<Listing> listings;
        log.info("Params: state={}", state);
        if (state != null) {
            listings = listingRepository.findByDealerAndState(dealer, state);
        } else {
            listings = listingRepository.findByDealer(dealer);
        }
        return listings.stream().map(ListingMapper::mapToListingResponse).toList();
    }

    @Override
    public ListingResponse updateListing(Long id, ListingRequest request) throws ListingNotFoundException {
        // Recupération du dealer actif
        Dealer dealer = dealerRepository.findByUsername(securityUtils.getCurrentUsername()).orElseThrow(() -> new RuntimeException("Une erreur est survenue. Merci de reessayer"));
        // On se rassure qu'il s'agit d'une resource de ce dealer
        Listing listing = listingRepository.findByIdAndDealer(id, dealer).orElseThrow(() -> new ListingNotFoundException(""));
        listing.setVehicle(request.getVehicle());
        listing.setPrice(request.getPrice());
        log.info("Listing modifié avec succès: {}", listing.getId());
        return ListingMapper.mapToListingResponse(listingRepository.save(listing));
    }

    @Override
    public String publishListing(Long id, PublishListingRequest request) {
        // Recupération du dealer actif
        Dealer dealer = dealerRepository.findByUsername(securityUtils.getCurrentUsername()).orElseThrow(() -> new RuntimeException("Une erreur est survenue. Merci de reessayer"));
        // On se rassure qu'il s'agit d'une resource de ce dealer
        Listing listing = listingRepository.findByIdAndDealer(id, dealer).orElseThrow(() -> new ListingNotFoundException(""));
        if (listing.getPublishedAt() == null) {
            int totalPublished = listingRepository.countByDealerAndState(dealer, ListingState.PUBLISHED);
            log.info("Dealer Limit: {}", dealer.getPublishedLimit());
            if (dealer.getPublishedLimit() > 0) {
                if (totalPublished < dealer.getPublishedLimit()) { // On verifie si la limite de publication n'est pas encore atteinte
                    listing.setState(ListingState.PUBLISHED);
                    listing.setPublishedAt(Date.from(Instant.now()));
                    log.info("Listing publié avec succès: {}", listing.getId());
                } else { // Si la limite de publication est atteinte
                    if (request.isUnpiblishOldOnLimitReached()) { // On verifie si le client souhaite depublier la plus ancienne publication
                        Listing oldPublishedListing = listingRepository.findOldPublishedAtByDealderId(dealer.getId()).orElseThrow(() -> new ListingLimitReachedException(""));
                        oldPublishedListing.setState(ListingState.DRAFT);
                        oldPublishedListing.setPublishedAt(null);
                        listingRepository.save(oldPublishedListing);
                        listing.setState(ListingState.PUBLISHED);
                        listing.setPublishedAt(Date.from(Instant.now()));
                        log.info("Listing {} publié avec succès et listing {} depublié avec succès", listing.getId(), oldPublishedListing.getId());
                    } else { // On retourne une erreur sinon
                        throw new ListingLimitReachedException("Vous avez atteint la limite de publication disponible : " + dealer.getPublishedLimit());
                    }
                }
                listingRepository.save(listing);
            } else {
                throw new ListingLimitReachedException("Vous ne pouvez pas faire de publication");
            }
        }
        return "Listing publié avec succès";
    }

    @Override
    public String unpublishListing(Long id) {
        // Recupération du dealer actif
        Dealer dealer = dealerRepository.findByUsername(securityUtils.getCurrentUsername()).orElseThrow(() -> new RuntimeException("Une erreur est survenue. Merci de reessayer"));
        // On se rassure qu'il s'agit d'une resource de ce dealer
        Listing listing = listingRepository.findByIdAndDealer(id, dealer).orElseThrow(() -> new ListingNotFoundException(""));
        if (listing.getPublishedAt() != null) {
            listing.setState(ListingState.DRAFT);
            listing.setPublishedAt(null);
            listingRepository.save(listing);
            log.info("Listing depublié avec succès: {}", listing.getId());
        }
        return "Listing depublié avec succès";
    }

    @Override
    public String destroyListing(Long id) throws ListingNotFoundException {
        // Recupération du dealer actif
        Dealer dealer = dealerRepository.findByUsername(securityUtils.getCurrentUsername()).orElseThrow(() -> new RuntimeException("Une erreur est survenue. Merci de reessayer"));
        // On se rassure qu'il s'agit d'une resource de ce dealer
        Listing listing = listingRepository.findByIdAndDealer(id, dealer).orElseThrow(() -> new ListingNotFoundException(""));
        listingRepository.delete(listing);
        log.info("Listing supprimé avec succès: {}", listing.getId());
        return "Listing supprimé avec succès";
    }
}
