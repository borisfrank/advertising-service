package com.borisfrank.advertisingservice.service.impl;

import com.borisfrank.advertisingservice.model.Dealer;
import com.borisfrank.advertisingservice.payload.request.LoginRequest;
import com.borisfrank.advertisingservice.payload.request.RegisterRequest;
import com.borisfrank.advertisingservice.payload.response.AuthResponse;
import com.borisfrank.advertisingservice.security.jwt.JwtUtils;
import com.borisfrank.advertisingservice.service.AuthService;
import com.borisfrank.advertisingservice.service.DealerService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final DealerService dealerService;
    private final JwtUtils jwtUtils;
    private final AuthenticationManager authenticationManager;

    @Override
    public AuthResponse login(LoginRequest request) {
        // On essaie d'authentifier l'utilisateur
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        // Authentification reussie, recuperation du dealer
        final Dealer dealer = dealerService.findByUsername(request.getUsername());
        return this.getJwtTokenFromUsername(dealer.getUsername());
    }

    @Override
    public AuthResponse register(RegisterRequest request) {
        Dealer dealer = dealerService.saveDealer(request);
        return this.getJwtTokenFromUsername(dealer.getUsername());
    }

    private AuthResponse getJwtTokenFromUsername(String username) {
        // Generation du token jwt à partir du username
        Map<String, String> idToken = jwtUtils.generateAccessAndRefreshTokens(username);
        return AuthResponse.builder()
                .accessToken(idToken.get("access_token"))
                .tokenType(idToken.get("token_type"))
                .build();
    }
}
