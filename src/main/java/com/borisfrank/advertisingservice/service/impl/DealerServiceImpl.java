package com.borisfrank.advertisingservice.service.impl;

import com.borisfrank.advertisingservice.config.AppConfig;
import com.borisfrank.advertisingservice.mapper.AuthMapper;
import com.borisfrank.advertisingservice.model.Dealer;
import com.borisfrank.advertisingservice.payload.request.RegisterRequest;
import com.borisfrank.advertisingservice.repository.DealerRepository;
import com.borisfrank.advertisingservice.service.DealerService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DealerServiceImpl implements DealerService {

    private final DealerRepository dealerRepository;
    private final PasswordEncoder passwordEncoder;
    private final AppConfig appConfig;

    @Override
    public boolean isUsernameUsed(String username) {
        return dealerRepository.existsByUsername(username);
    }

    @Override
    public Dealer saveDealer(RegisterRequest request) {
        Dealer dealer = AuthMapper.mapToDealer(request);
        // On encode le mot de passe à l'aide de BCrypt
        dealer.setPassword(passwordEncoder.encode(request.getPassword()));
        dealer.setPublishedLimit(appConfig.getDefaultPublishedLimit());
        return dealerRepository.save(dealer);
    }

    @Override
    public Dealer findByUsername(String username) throws UsernameNotFoundException {
        return dealerRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Aucun compte pour le username: " + username));
    }
}
