package com.borisfrank.advertisingservice.service;

import com.borisfrank.advertisingservice.exception.ListingNotFoundException;
import com.borisfrank.advertisingservice.model.ListingState;
import com.borisfrank.advertisingservice.payload.request.ListingRequest;
import com.borisfrank.advertisingservice.payload.request.PublishListingRequest;
import com.borisfrank.advertisingservice.payload.response.ListingResponse;

import java.util.List;

public interface ListingService {
    ListingResponse saveListing(ListingRequest request);
    List<ListingResponse> getAllListing(ListingState state);
    ListingResponse updateListing(Long id, ListingRequest request) throws ListingNotFoundException;
    String publishListing(Long id, PublishListingRequest request) throws ListingNotFoundException;
    String unpublishListing(Long id) throws ListingNotFoundException;
    String destroyListing(Long id) throws ListingNotFoundException;
}
