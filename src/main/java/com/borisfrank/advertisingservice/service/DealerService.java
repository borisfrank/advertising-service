package com.borisfrank.advertisingservice.service;

import com.borisfrank.advertisingservice.model.Dealer;
import com.borisfrank.advertisingservice.payload.request.RegisterRequest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface DealerService {
    boolean isUsernameUsed(String username);
    Dealer saveDealer(RegisterRequest request);
    Dealer findByUsername(String username) throws UsernameNotFoundException;
}
