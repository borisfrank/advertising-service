package com.borisfrank.advertisingservice;

import com.borisfrank.advertisingservice.controller.ListingController;
import com.borisfrank.advertisingservice.mapper.ListingMapper;
import com.borisfrank.advertisingservice.model.Listing;
import com.borisfrank.advertisingservice.payload.request.ListingRequest;
import com.borisfrank.advertisingservice.payload.response.ListingResponse;
import com.borisfrank.advertisingservice.service.impl.ListingServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ListingControllerTest {

    @InjectMocks
    private ListingController controller;

    @Mock
    private ListingServiceImpl listingService;

    @Test
    void shouldAddNewListing() {
        ListingRequest request = ListingRequest.builder().vehicle("Toyota").price(1223.5).build();
        Listing listing = Listing.builder().id(1L).vehicle("Toyota").price(1223.5).build();
        ListingResponse response = ListingMapper.mapToListingResponse(listing);
        when(listingService.saveListing(any(ListingRequest.class))).thenReturn(response);

        ResponseEntity<ListingResponse> responseEntity = controller.saveListing(request);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(responseEntity.getBody()).isNotEqualTo(null);
    }
}
