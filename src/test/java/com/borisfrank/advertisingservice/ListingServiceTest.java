package com.borisfrank.advertisingservice;

import com.borisfrank.advertisingservice.mapper.ListingMapper;
import com.borisfrank.advertisingservice.model.Dealer;
import com.borisfrank.advertisingservice.model.Listing;
import com.borisfrank.advertisingservice.payload.request.ListingRequest;
import com.borisfrank.advertisingservice.payload.response.ListingResponse;
import com.borisfrank.advertisingservice.repository.DealerRepository;
import com.borisfrank.advertisingservice.repository.ListingRepository;
import com.borisfrank.advertisingservice.service.impl.ListingServiceImpl;
import com.borisfrank.advertisingservice.util.WebSecurityUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ListingServiceTest {
    @Mock
    private ListingRepository listingRepository;
    @Mock
    private DealerRepository dealerRepository;
    @Mock
    private Dealer dealerStub;
    @Mock
    private WebSecurityUtils securityUtils;

    @InjectMocks
    private ListingServiceImpl listingService;

    @Test
    void saveListing_Success() {
        ListingRequest request = ListingRequest.builder().vehicle("Toyota").price(1223.5).build();
        Listing listing = Listing.builder().id(1L).vehicle("Toyota").price(1223.5).build();
        ListingResponse response = ListingMapper.mapToListingResponse(listing);

        when(securityUtils.getCurrentUsername()).thenReturn("frank");
        when(dealerRepository.findByUsername(any(String.class))).thenReturn(Optional.of(dealerStub));
        when(listingRepository.save(any(Listing.class))).thenReturn(listing);

        ListingResponse savedResponse = listingService.saveListing(request);
        assertThat(savedResponse.getVehicle()).isEqualTo(response.getVehicle());
    }

    @Test
    void updateListing_Success() {
        ListingRequest request = ListingRequest.builder().vehicle("Toyota").price(1223.5).build();
        Listing listing = Listing.builder().id(1L).vehicle("Toyota").price(1223.5).build();
        ListingResponse response = ListingMapper.mapToListingResponse(listing);

        when(securityUtils.getCurrentUsername()).thenReturn("frank");
        when(dealerRepository.findByUsername(any(String.class))).thenReturn(Optional.of(dealerStub));
        when(listingRepository.findByIdAndDealer(1L, dealerStub)).thenReturn(Optional.ofNullable(listing));
        when(listingRepository.save(any(Listing.class))).thenReturn(listing);

        ListingResponse savedResponse = listingService.updateListing(1L, request);
        assertThat(savedResponse.getVehicle()).isEqualTo(response.getVehicle());
    }
}
