package com.borisfrank.advertisingservice;


import com.borisfrank.advertisingservice.util.WebSecurityUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WebSecurityUtilsTest {
    @Mock
    private WebSecurityUtils securityUtils;

    private final String defaultName = "frank";

    @Test
    void getCurrentUsername_Success() {
        when(securityUtils.getCurrentUsername()).thenReturn(defaultName);
        assertThat(securityUtils.getCurrentUsername()).isEqualTo(defaultName);
    }
}
